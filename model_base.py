import os
import sys
import random
import math
import numpy as np
import skimage.io
import matplotlib
import matplotlib.pyplot as plt
from pycocotools.coco import COCO
from mrcnn.config import Config
from keras.engine import saving
from mrcnn import utils
import mrcnn.model as modellib
from mrcnn import visualize
import coco
import helpers
import warnings
warnings.filterwarnings("ignore")

#import tensorflow.compat.v1 as tf
#tf.disable_v2_behavior()
# Root directory of the project
# ROOT_DIR = os.path.abspath("../")
# print(ROOT_DIR)


def get_model():

    ROOT_DIR = os.path.abspath("Mask_RCNN-master")




    #Import Mask RCNN
    sys.path.append(ROOT_DIR)  # To find local version of the library

    #Import COCO config
    sys.path.append(os.path.join(ROOT_DIR, "samples/coco/"))  # To find local version


    # %matplotlib inline

    # Directory to save logs and trained model
    MODEL_DIR = os.path.join(ROOT_DIR, "logs")

    # Local path to trained weights file
    COCO_MODEL_PATH = os.path.join('', "mask_rcnn_coco.h5")

    # Download COCO trained weights from Releases if needed
    if not os.path.exists(COCO_MODEL_PATH):
        utils.download_trained_weights(COCO_MODEL_PATH)

    # Directory of images to run detection on
    #IMAGE_DIR = os.path.join(ROOT_DIR, "images")

    class InferenceConfig(coco.CocoConfig):
    #class InferenceConfig(coco.CocoConfig):
        # Set batch size to 1 since we'll be running inference on
        # one image at a time. Batch size = GPU_COUNT * IMAGES_PER_GPU
        GPU_COUNT = 1
        IMAGES_PER_GPU = 1

    config = InferenceConfig()
    config.display()



    model = modellib.MaskRCNN(mode="inference", config=config, model_dir=ROOT_DIR)
    # Load weights trained on MS-COCO

    model.load_weights(COCO_MODEL_PATH, by_name=True)

    return model