import requests

BASE = "http://127.0.0.1:5000/"



input_data = {
"categorySchema":{
    "1619164979041":{
    "description": "car",
    "name": "Arabalar",
    "tool": "bounding-box"
    },
    "1619165092190":{
    "description": "person",
    "name": "Yayalar",
    "tool": "polygon"
    }
},
"data": "https://angohub.s3.us-east-2.amazonaws.com/uploaded-data--1619164931143.jpg"
}

# keys = list(input_data['categorySchema'].keys()) 
# for key in keys:
#     print(input_data['categorySchema'][key])


response = requests.get(BASE + "detect", json=input_data)
print("DATA RETURNED!: ")
print(response.json())