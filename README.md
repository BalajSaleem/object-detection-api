# OBJECT DETECTION FLASK

## About
A microservice intended to assist image annotation.

Implemented using flask for restful API and MaskRCNN for object detection.

Creates an intance of the model when server is setup and uses the model to detect object in images provided.




## How to Set Up Server
*   Install libraries from requirements.txt (Keras version 2.2.0, and Tensorflow version 1.4 were used for testing)
*   Run "python3 app.py" to start the flask microservice and setup the model.
*   The server will be hosted on:  http://127.0.0.1:5000/

## How to Interact With The Server
A GET request should be made to http://127.0.0.1:5000/detect with the following (json) body (just an example) :


{

"categorySchema":{

    "1619164979041":{

    "description": "car",

    "name": "Arabalar",

    "tool": "bounding-box"

    },

    "1619165092190":{

    "description": "person",

    "name": "Yayalar",

    "tool": "polygon"

    }

},

"data": "https://angohub.s3.us-east-2.amazonaws.com/uploaded-data--1619164931143.jpg"

}


You can also run python3 test.py for a demonstration of this


For details of requests and the outputs please refer to the following document:

https://docs.google.com/document/d/1kMyfX1AxB56mswP9qL9USCVUdmw7wRRIGIcb79Oklkg/edit



## Libaries used
*   numpy
*   scipy
*   Pillow
*   cython
*   matplotlib
*   scikit-image
*   tensorflow>=1.3.0
*   keras>=2.0.8
*   opencv-python
*   h5py
*   flask
*   flask-restful
*   requests
*   shutil
*   imgaug
*   IPython[all]


## Authors

Ango AI (Balaj Saleem , Gokalp Urul)