def mask_to_polygon(mask):
      #mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY )
  contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
  polygons = []

  for object in contours:
    coords = []
    
    for point in object:
        # coords.append(int(point[0][0]))
        # coords.append(int(point[0][1]))
        coords = (int(point[0][0]) , int(point[0][1]))

    polygons.append(coords)
  print(polygons)
  return polygons