import cv2
import model_base
import os
import skimage.io
import numpy as np
from mrcnn import visualize

class_names = ['BG', 'person', 'bicycle', 'car', 'motorcycle', 'airplane',
               'bus', 'train', 'truck', 'boat', 'traffic light',
               'fire hydrant', 'stop sign', 'parking meter', 'bench', 'bird',
               'cat', 'dog', 'horse', 'sheep', 'cow', 'elephant', 'bear',
               'zebra', 'giraffe', 'backpack', 'umbrella', 'handbag', 'tie',
               'suitcase', 'frisbee', 'skis', 'snowboard', 'sports ball',
               'kite', 'baseball bat', 'baseball glove', 'skateboard',
               'surfboard', 'tennis racket', 'bottle', 'wine glass', 'cup',
               'fork', 'knife', 'spoon', 'bowl', 'banana', 'apple',
               'sandwich', 'orange', 'broccoli', 'carrot', 'hot dog', 'pizza',
               'donut', 'cake', 'chair', 'couch', 'potted plant', 'bed',
               'dining table', 'toilet', 'tv', 'laptop', 'mouse', 'remote',
               'keyboard', 'cell phone', 'microwave', 'oven', 'toaster',
               'sink', 'refrigerator', 'book', 'clock', 'vase', 'scissors',
               'teddy bear', 'hair drier', 'toothbrush']

def mask_to_polygon(mask):
      #mask = cv2.cvtColor(mask, cv2.COLOR_BGR2GRAY )
  contours, _ = cv2.findContours(mask, cv2.RETR_TREE, cv2.CHAIN_APPROX_NONE)
  polygons = []

  for object in contours:
    #coords = []
    
    for point in object:
        # coords.append(int(point[0][0]))
        # coords.append(int(point[0][1]))
        polygons.append([int(point[0][0]) , int(point[0][1])])

    #polygons.append(coords)
  return np.array(polygons)




def get_cordinates(model, class_name = 'car', tool = 'bounding-box', image_name='my_image.png', show = False, pre_results = None):
  image = skimage.io.imread(os.path.join(image_name))
  # Run detection
  if pre_results == None:
    results = model.detect([image], verbose=1)
  else:
    results =  pre_results
  # Filter results
  r = results[0]
  print( [class_names[i] for i in r['class_ids']]  )
  filter_class = class_name
  filter_class_index = class_names.index(filter_class)
  filtered_indexes = [ i for i, x in enumerate(r['class_ids']) if x == filter_class_index]
  # filtered_indexes = filtered_indexes[0:1]
  filtered_rois = np.array(r['rois'][filtered_indexes,:])
  filtered_masks =  np.array(r['masks'][:,:, filtered_indexes])
  filtered_scores =  np.array(r['scores'][filtered_indexes])
  filtered_ids = np.array(r['class_ids'][filtered_indexes]) 
  print(f"total filtered objects of label {filter_class}: {len(filtered_indexes)}")

  #print(f"Filtered Shape : {filtered_masks.shape} ; original shape : {r['masks'].shape}" )

  # visualize.display_instances(image, r['rois'] , r['masks'], r['class_ids'], class_names, r['scores'], title='Detected Objects')


  if show:
    visualize.display_instances(image,filtered_rois , filtered_masks, filtered_ids, class_names, filtered_scores, title='Filtered ' + filter_class + ' Objects')

  polygons = []
  for i in range(filtered_masks.shape[2]):
      my_mask = np.array( filtered_masks[:,:,i], dtype=np.uint8)
      points = mask_to_polygon(my_mask)
      polygons.append(points)
  polygons = np.array(polygons)
  #polygons = np.concatenate( polygons, axis=0 )

  # print(f"Filtered Bounding Boxes: {filtered_rois.shape}")
  # print(f"Filtered Polygons: {polygons.shape}")

  if tool == 'bounding-box':
        return filtered_rois, results
  else:
        return polygons, results
