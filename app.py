from flask import Flask, request, jsonify
from flask_restful import Api, Resource
import shutil
import requests
import model_base
import helpers
import os
import skimage.io
import numpy as np

app = Flask(__name__)
api = Api(app)
model = model_base.get_model()
pre_results = None
prev_img_url = ''


class Query:
    def __init__(self,id, description, name, tool):
        self.id = id
        self.description = description
        self.name = name
        self.tool = tool

    def __str__(self):
        return 'id: ' + self.id + ' description: ' + self.description + ' name: ' + self.name + ' tool: ' +self.tool

class HomePage(Resource):
    def get(self):
        global model
        global pre_results
        global prev_img_url
        data = request.json
        imgUrl, queries = decodeData(data)
        if prev_img_url != imgUrl:
            getImage(imgUrl)
            pre_results = None
            prev_img_url = imgUrl

        results = process_queries(queries)
        json = encode_results( queries , results )
        return (json) 

api.add_resource(HomePage, "/detect")

def process_queries(queries):
    results = []
    for query in queries:
        results.append( get_result(query))
    return results


def decodeData(input_data):
    imgUrl = input_data['data']
    queries = []
    keys = list(input_data['categorySchema'].keys()) 
    for key in keys:
        #print(input_data['categorySchema'][key])
        queries.append( Query(key, input_data['categorySchema'][key]['description'], input_data['categorySchema'][key]['name'], input_data['categorySchema'][key]['tool'])  )
    return imgUrl, queries


def get_result(query, show=False):
    global pre_results
    print('getting cordinates...')
    cords , r = helpers.get_cordinates(model, class_name= query.description  , tool=query.tool , show=show, pre_results=pre_results)
    pre_results = r
    return cords
    print('cordinates received')


def encode_results(queries, results):

    json = {
        "results" : {

        }
    }

    for i in range(len(queries)):
        query = queries[i]
        result = results[i]
        if(query.tool == 'bounding-box'):
            json['results'][query.id] = all_bbs_to_dict(result)
        else:
            json['results'][query.id] = all_polygons_to_dict(result)

    return json

def bb_to_dict(bb):
    return {
        "x": int( bb[0]),
        "y": int(bb[1]) ,
        "width": int(bb[2]) ,
        "height": int(bb[3]) ,
        "tool": "bounding-box"
    }

def all_bbs_to_dict(bbs):
    answer = []
    for bb in bbs:
        answer.append(bb_to_dict(bb))
    return {"answer": answer}

def polygon_to_dict(polygon):
    points = []
    for point in polygon:
        points.append (  [ int(point[0]) , int(point[1])   ] )
    return {"points": points, "tool" : "polygon"}

def all_polygons_to_dict(polygons):
    answer = []
    for polygon in polygons:
        answer.append(polygon_to_dict(polygon))
    return {"answer": answer}

def getImage(imgUrl):
    print("getting image...")
    response = requests.get(imgUrl, stream=True)
    with open('my_image.png', 'wb') as out_file:
        shutil.copyfileobj(response.raw, out_file)
    del response
    print("image downloaded.")



if __name__ == "__main__":
    
    app.run(threaded=False)